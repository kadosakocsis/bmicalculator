package Exercise.firstExcercise;

public class Calculator {
	private static String checkBMI(double r) {
		if(r < 16) {
			return "(Severe Thinness)";
		} else if (r >= 16 && r < 17) {
			return "(Moderate Thinness)";
		} else if (r >= 17 && r < 18.5) {
			return "(Mild Thinness)";
		} else if (r >= 18.5 && r < 25) {
			return "(Normal)";
		} else if (r >= 25 && r < 30) {
			return "(Overweight)";
		} else if (r >= 30 && r < 35) {
			return "(Obese Class I)";
		} else if (r >= 35 && r < 40) {
			return "(Obese Class II)";
		} else {
			return "(Obese Class III)";
		}
	}

	static String calculateBMI(Person p)  {
		if(p.getHeight() <= 0.0 || p.getWeight() <= 0.0) {
			return "BMI couldn't be calculated due to invalid value.";
		} else {
			double result = p.getWeight() / Math.pow(p.getHeight(), 2);
			return "BMI = " + String.format("%.2f ", result) + checkBMI(result);
		}
	}
}
