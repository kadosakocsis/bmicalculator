package Exercise.firstExcercise;

public class Person {
	
	private double height; //Person's height is measured in m
	private double weight; //Person's weight is measured in kg
	
	public Person() {
		this.height = 0;
		this.weight = 0;
	}
	public Person(double height, double weight) {
		if(height < 2.3 && height > 0.5) {
			this.height = height;
		} else {
			this.height = 0;
			throw new IllegalArgumentException("Too tall or short!");
		}
		if (weight > 2.5) {
			this.weight = weight;
		} else {
			throw new IllegalArgumentException("Too light!");
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
}

