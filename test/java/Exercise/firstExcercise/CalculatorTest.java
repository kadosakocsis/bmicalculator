package Exercise.firstExcercise;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest extends Calculator {
	private Person[] individual = new Person[10]; 
	@Before
	public void setUp() throws Exception{
		individual[0] = new Person();
		individual[1] = new Person(1.8, 50);
		individual[2] = new Person(1.8, 55);
		individual[3] = new Person(1.8, 56);
		individual[4] = new Person(1.8, 65);
		individual[5] = new Person(1.8, 70);
		individual[6] = new Person(1.8, 88);
		individual[7] = new Person(1.8, 100);
		individual[8] = new Person(1.8, 120);
		individual[9] = new Person(1.8, 130);
	}
	
	@Test
	public void testIfBMITableIsCorrect() {
		String result1 = Calculator.calculateBMI(individual[0]);
		String result2 = Calculator.calculateBMI(individual[1]);
		String result3 = Calculator.calculateBMI(individual[2]);
		String result4 = Calculator.calculateBMI(individual[3]);
		String result5 = Calculator.calculateBMI(individual[4]);
		String result6 = Calculator.calculateBMI(individual[5]);
		String result7 = Calculator.calculateBMI(individual[6]);
		String result8 = Calculator.calculateBMI(individual[7]);
		String result9 = Calculator.calculateBMI(individual[8]);
		String result10 = Calculator.calculateBMI(individual[9]);
		
		assertEquals("BMI couldn't be calculated due to invalid value.", result1);
		assertEquals("BMI = 15.43 (Severe Thinness)", result2);
		assertEquals("BMI = 16.98 (Moderate Thinness)", result3);
		assertEquals("BMI = 17.28 (Mild Thinness)", result4);
		assertEquals("BMI = 20.06 (Normal)", result5);
		assertEquals("BMI = 21.60 (Normal)", result6);
		assertEquals("BMI = 27.16 (Overweight)", result7);
		assertEquals("BMI = 30.86 (Obese Class I)", result8);
		assertEquals("BMI = 37.04 (Obese Class II)", result9);
		assertEquals("BMI = 40.12 (Obese Class III)", result10);
	}

}
